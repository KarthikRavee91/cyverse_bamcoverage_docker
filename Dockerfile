FROM  bgruening/galaxy-stable

COPY . /app
WORKDIR /app
RUN chmod +x igb_tbi_creator.sh
RUN apt-get -y update;exit 0
RUN apt-get install -y gcc
RUN apt-get install -y apt-utils
RUN apt-get install -y make
RUN apt-get install -y libbz2-dev
RUN apt-get install -y zlib1g-dev
RUN apt-get install -y libncurses5-dev
RUN apt-get install -y libncursesw5-dev
RUN apt-get install -y liblzma-dev
RUN apt-get install -y wget
RUN apt-get install -y curl
RUN apt-get install -y bzip2
RUN apt-get install -y python-pip
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

RUN wget https://github.com/samtools/htslib/releases/download/1.3.2/htslib-1.3.2.tar.bz2 -O htslib.tar.bz2
RUN tar -xjvf htslib.tar.bz2
WORKDIR /app/htslib-1.3.2
RUN make && make install
RUN export PATH="/app/htslib-1.14:$PATH"

WORKDIR /app

