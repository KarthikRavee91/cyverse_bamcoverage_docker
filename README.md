# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Create a Cyverse app to create RNA-seq coverage graph
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* To build the docker image with the tag 'deeptools'
```docker build -f ./Dockerfile -t deeptools .```

* To create and mount host directory and run the docker container, 'demo'
```docker run -d -t -v <host_directory_absolute_path>:/app --name demo lorainelab/deeptools-for-cyverse```

* To see the containers that's created
```docker ps -a```

* To execute the igb_tbi_creator.sh with input filename
```docker exec demo ./igb_tbi_creator.sh sample_files/BedGraph_HomoSapien.bedGraph```

* To debug the container and run the script
```docker exec -it demo ```
```./igb_tbi_creator.sh sample_files/BedGraph_HomoSapien.bedGraph'''

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact