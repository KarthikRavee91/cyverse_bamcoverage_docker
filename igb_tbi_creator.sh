#!/bin/bash
fp="${1%%.*}"
bamCoverage -b $1 --outFileFormat bedgraph -o "$fp.bedGraph"
sort -k1,1 -k2,2n "$fp.bedGraph" | bgzip > "$fp.bedGraph.gz"
tabix -s 1 -b 2 -e 3 "$fp.bedGraph.gz"
#/app/bioviz-connect/BedGraph_HomoSapien.bedGraph